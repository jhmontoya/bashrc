alias tree="git log --all --graph --decorate --oneline"
alias cdp="cd /c/Guidewire/PolicyCenter/modules/"
alias gcd="git checkout develop"
alias gcq="git checkout quality"
alias gc="git checkout $1"
alias gcb="git checkout -b $1"
alias rmb="git branch -D $1"
alias gs="git status"
alias gcp="git cherry-pick $1"
alias c="clear"
alias gfo="git fetch origin"
alias cdgs="cd /c/Guidewire/PolicyCenter/modules/configuration/gsrc/sura/suite"
alias cdgt="cd /c/Guidewire/PolicyCenter/modules/configuration/gtest/sura/suite"
alias cdb="cd ~/bashrc"
alias nt="/c/Program\ Files/Notepad++/notepad++.exe"
	
bind '"\C-w"':"\"git checkout develop && git pull \C-m\""

cdp	